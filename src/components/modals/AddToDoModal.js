import React, {useState} from "react";
import './styles.scss';
import {toDoActions} from "../../_actions";
import {connect} from 'react-redux';

const AddToDoModal = (props) => {
    const {dispatch} = props;
    const [title, handleTitle] = useState('');
    const [body, handleBody] = useState('');
    const [color, handleColor] = useState('orange');

    const [wrapperRef, setWrapperRef] = useState();

    const closeOutsideModal = (e) => {
        if (wrapperRef && !wrapperRef.contains(e.target)) {
            props.close();
        }
    };

    return (
        <div onClick={closeOutsideModal} className="modal-wrapper">
            <div ref={setWrapperRef} className="modal-content">
                <div className="modal-header">
                    <h2 className="header-title">Создание задания</h2>
                    <a onClick={() => {
                        props.close(false);
                    }} className="header-close-modal"> </a>
                </div>
                <div className="modal-body">
                    <form className="body-form">
                        <input placeholder="Введите название" className="form-input" onChange={(event) => {
                            handleTitle(event.target.value);
                        }} type="text"/>

                        <input placeholder="Введите задание" className="form-input" onChange={(event) => {
                            handleBody(event.target.value);
                        }} type="text"/>

                        <button className="form-button" onClick={(e) => {
                            e.preventDefault();
                            if (title.length > 0 && body.length > 0) {
                                dispatch(toDoActions.addToDo(title, body, color));
                                props.close(false);
                            }
                        }}>Создать
                        </button>

                        <div className="change-color">
                            <input onClick={() => {
                                handleColor("orange");
                            }} name="choise-color" id="orange-radio" className="ratio-input" type="radio"/>
                            <label htmlFor="orange-radio"></label>

                            <input onClick={() => {
                                handleColor("purple");
                            }} name="choise-color" id="purple-radio" className="ratio-input" type="radio"/>
                            <label htmlFor="purple-radio"></label>

                            <input onClick={() => {
                                handleColor("red");
                            }} name="choise-color" id="red-radio" className="ratio-input" type="radio"/>
                            <label htmlFor="red-radio"></label>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    )
};

const mapStateToProps = (state) => {
    const {toDo} = state;

    return {
        toDo
    }
};

export default connect(mapStateToProps)(AddToDoModal);