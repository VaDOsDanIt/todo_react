import * as constants from '../_constants/index';

const addToDo = (title, body, color) => {
    return {
        type: constants.ADD_TO_DO,
        payload: {
            title,
            color,
            body
        }
    }
};

const removeToDo = (id) => {
    return {
        type: constants.REMOVE_TO_DO,
        payload: {
            id
        }
    }
};

const editToDo = () => {
    return {
        type: constants.EDIT_TO_DO,
    }
};

export const toDoActions = {
    addToDo,
    removeToDo,
    editToDo
};