import * as constants from "../_constants/index";

const initialState = {
    count: 0
};

export const Counter = (state = initialState, action) => {
    switch (action.type) {
        case constants.COUNTER_ADD:
            return {
                count: state.count + action.payload,
            };

        case constants.COUNTER_REMOVE:
            return {
                count: state.count - action.payload,
            };
            
        default:
            return state;
    }
};