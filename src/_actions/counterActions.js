import * as constant from '../_constants/index';

const add = () => {
    return {
        type: constant.COUNTER_ADD,
        payload: 1
    }
};

const remove = () => {
    return {
        type: constant.COUNTER_REMOVE,
        payload: 1
    }
};

export const counterActions = {
    add,
    remove
};