import {combineReducers} from "redux";

import {Counter} from "./counterReducer";
import {toDo} from "./toDoReducers";

export const reducers = combineReducers({Counter, toDo});