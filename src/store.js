import {createStore} from "redux";
import {reducers} from "./_reducers/combineReducers";


export const store = createStore(reducers);