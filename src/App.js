import React, {useState} from 'react';
import {connect} from "react-redux";
import {counterActions, toDoActions} from './_actions';
import PropTypes from 'prop-types';
import AddToDoModal from "./components/modals/AddToDoModal";
import './scss/style.scss';
import ToDoElement from "./components/ToDoElement";


function App(props) {
    const {dispatch, toDo} = props;

    const [isOpenModal, handleOpenModal] = useState(false);


    return (
        <div className="App">

            <button className="create-task-button" onClick={(event) => {
                event.preventDefault();
                handleOpenModal(true);
            }}>Создать
            </button>

            {isOpenModal &&
            <AddToDoModal
                close={handleOpenModal}
            />}

            {toDo.toDoList.map((item, i) =>
                <ToDoElement
                i={i}
                item={item}
                />
            )}
        </div>
    );
}


App.propTypes = {
    count: PropTypes.number
};


function mapStateToProps(state) {
    const {Counter, toDo} = state;
    return {
        Counter,
        toDo
    };
}

export default connect(mapStateToProps)(App);
