import moment from "moment";
import {toDoActions} from "../_actions";
import React from "react";
import {connect} from 'react-redux';

const ToDoElement = (props) => {
    const {item, i, dispatch} = props;

    const deleteToDo = () => {
        dispatch(toDoActions.removeToDo(item.id));
    };

    return (
        <ul className={"task " + (item.color) + "-background"} key={i}>
            <li className="task-date">{moment(item.date).format("DD.MM.yyyy")}</li>
            <li className="task-title">{item.title}</li>
            <li className="task-body">{item.body}</li>
            <button onClick={deleteToDo} className="delete-card"></button>
        </ul>
    )
};

function mapStateToProps(state) {
    const {Counter, toDo} = state;
    return {
        Counter,
        toDo
    };
}

export default connect(mapStateToProps)(ToDoElement);