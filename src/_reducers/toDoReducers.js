import * as constants from "../_constants/index";

const initialState = JSON.parse(localStorage.getItem('todos')) || {
    toDoList: [],
};

export const toDo = (state = initialState, action) => {
    switch (action.type) {
        case constants.ADD_TO_DO:
            localStorage.setItem('todos', JSON.stringify({
                toDoList: [...state.toDoList, {
                    date: new Date(),
                    id: `f${(+new Date).toString(16)}`,
                    color: action.payload.color,
                    title: action.payload.title,
                    body: action.payload.body
                }]
                }));
            return {
                toDoList: [...state.toDoList, {
                    date: new Date(),
                    id: `f${(+new Date).toString(16)}`,
                    color: action.payload.color,
                    title: action.payload.title,
                    body: action.payload.body
                }]
            };

        case constants.REMOVE_TO_DO:

            localStorage.setItem('todos', JSON.stringify({
                toDoList: state.toDoList.filter(item => {
                    if (item.id !== action.payload.id) {
                        return item;
                    }
                })
            }));

            return {
                toDoList: state.toDoList.filter(item => {
                  if (item.id !== action.payload.id) {
                      return item;
                  }
                })
            }
        default:
            return state;
    }
};